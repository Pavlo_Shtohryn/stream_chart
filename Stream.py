import random
import pylab as plt
################################################################################################
lengthVoice = 100
lengthVideo = 600
lengthData = 400
TIME = []
STREAM = []
LAMBDAS = [] 
CPS = []
C = []
iter=1
time = 1
jter = 0
################################################################################################
while iter <= 10:
    while jter != 14400:
        p1 = random.random()
        p2 = random.random()
        p3 = random.random()
        lyamdVoice = random.uniform(0, 10)
        lyamdVideo = random.uniform(0, 20)
        lyamdData = random.uniform(0, 40)
        poison = lyamdVoice*p1*lengthVoice + lyamdVideo*p2*lengthVideo + lyamdData*p3*lengthData
        LAMBDAS = LAMBDAS + [int(poison)]
        jter += 1
    while time != 14401:
        TIME = TIME + [int(time)]
        time += 1
    STREAM = STREAM + [LAMBDAS]
    iter += 1
    jter = 0
    LAMBDAS = []
CPS = [sum(k) for k in zip(*STREAM)]
while jter != 14400:
    C = C + [int(0.805*max(CPS))]
    jter += 1
plt.figure(1)
plt.plot(TIME, STREAM[0], TIME, STREAM[1], TIME, STREAM[2], TIME, STREAM[3], TIME, STREAM[4],
 TIME, STREAM[5], TIME, STREAM[6], TIME, STREAM[7], TIME, STREAM[8], TIME, STREAM[9])
plt.grid(True)
plt.figure(2)
plt.plot(TIME, CPS, TIME, C)
plt.grid(True)
plt.show()